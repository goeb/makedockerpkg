PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
DOCDIR ?= $(PREFIX)/share/doc/makedockerpkg
MANDIR ?= $(PREFIX)/share/man

doc: $(basename $(wildcard doc/*.rst))

clean:

	rm -f doc/makedockerpkg.1

install: doc

	install -d $(DESTDIR)$(BINDIR)
	install -t $(DESTDIR)$(BINDIR)      -m 755 bin/makedockerpkg

	install -d $(DESTDIR)$(MANDIR)/man1
	install -t $(DESTDIR)$(MANDIR)/man1 -m 644 doc/makedockerpkg.1

	install -d $(DESTDIR)$(DOCDIR)
	install -t $(DESTDIR)$(DOCDIR)      -m 644 README

uninstall:

	rm -f  $(BINDIR)/makedockerpkg
	rm -f  $(MANDIR)/man1/makedockerpkg.1
	rm -rf $(DOCDIR)

doc/%: doc/%.rst

	rst2man '$<' >'$@'

.PHONY: clean doc install uninstall

# :indentSize=3:tabSize=3:noTabs=false:mode=makefile:maxLineLen=78:

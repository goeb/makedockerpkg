==============================================================================
makedockerpkg
==============================================================================

------------------------------------------------------------------------------
Build Arch Linux packages in Docker containers.
------------------------------------------------------------------------------

:Author:         Stefan Göbel <mkdkpk ʇɐ subtype ˙ de >
:Date:           2019/02/28
:Version:        0.3
:Manual section: 1
:Manual group:   User Commands

SYNOPSIS
==============================================================================

**makedockerpkg** *[<option>…]* *[-- <makepkg option>… [<ENVVAR>[+]=<value>]]*

DESCRIPTION
==============================================================================

Similar to Arch Linux' **makechrootpkg**, **makedockerpkg** may be used to
build an Arch Linux package in a (mostly) clean environment, inside a Docker
container instead of a chroot.

See the *DETAILS* section below for more information, preferably before
running **makedockerpkg**.

OPTIONS
==============================================================================

**makepkg** options may be specified after the **makedockerpkg** options,
separated by `--` (`--` is also required if only **makepkg** options are set).
**makepkg** options may include variable assignments (*[<ENVVAR>[+]=<value>]*)
to overwrite (or append the value to) existing environment variables.

-b <name>, --build-image <name>

   Name of the package-specific image to be used. May include a tag. The
   default value is `makedockerpkg/` followed by the basename of the working
   directory, and if no tag is set it defaults to `latest`. The image will be
   build if it does not exist or requires an update.

-B <name>, --base-image <name>

   Name of the base image to be used. May include a tag. The default value is
   `makedockerpkg/base-devel`, and if no tag is set it defaults to `latest`.
   The image will be build if it does not exist or requires an update.

   *Note:* Do not use any other tag than `latest` unless you know what you're
   doing!

-c <file>, --makepkg-conf <file>

   Location of the **makepkg** configuration file. If not set, or if it is set
   to `/etc/makepkg.conf`, the default location(s) will be used (i.e. both the
   system-wide configuration and the user configuration will be respected).
   For any other value only the specified file will be used.

-C <file>, --pacman-conf <file>

   Location of the **pacman** configuration file. The default value is
   `/etc/pacman.conf`. Note that this file will not be used directly, instead
   **pacman-conf** will be used to dump the configuration from the specified
   file (this will also process any included files, like the mirror list).

-d, --debug

   Print some status messages. If not specified, **makedockerpkg** will not
   print any of its own messages. May be specified more than once to increase
   the verbosity level.

-D <directory>, --base-dir <directory>

   Directory to use as the build context when the base image is created. If
   not set, a new temporary directory will be created (using **mktemp**).
   Note: If set, the directory will be created if necessary, and it will not
   be removed automatically. If not set, the temporary directory will be
   removed when it is no longer required.

-f, --force-build

   Force a rebuild of the package-specific Docker image.

-F, --force-base

   Force a rebuild of the base image.

-g, --gpg-mount

   Mount `$GNUPGHOME` in the build container. The `$GNUPGHOME` environment
   variable must not be empty (even if the default location is used),
   otherwise this option will be ignored. Values set in `makepkg.conf` will be
   respected. The directory will be writable from inside the container!

-G, --pacman-keys

   Mount the host's **pacman** keyring in the container. This may be required
   if custom keys have been added to the keyring. The keyring will be mounted
   read-only in the container.

-h, --help

   Print a help message / usage information.

-m <mount>, --mount <mount>

   Additional mounts to use when running the build container. The `<mount>`
   option format is the same as for `docker run`'s `--mount` option, see
   *docker-run*\ (1) for more details.

-o, --no-color

   Disable colored output for the `--debug` option (affects only the output of
   **makedockerpkg** itself).

-p <package>, --build-pkg <package>

   Additional package to install in the package-specific build image. By
   default, all dependencies will be installed. Using this option will add
   another package, i.e. the dependencies will still be installed. Only
   repositories in the **pacman** configuration (see `--pacman-conf`) will be
   used to install packages, dependencies from the AUR will not be built
   automatically! This option may be specified multiple times.

-P <package>, --base-pkg <package>

   Package(s) to install in the base image. The default is to install only the
   `base-devel` group (and its dependencies, obviously). Note that the `base`
   group will *NOT* be installed by default (except packages pulled in as
   dependencies for `base-devel`), if a package from `base` is missing for a
   build it is recommended to use the `--build-pkg` option to add it to the
   package-specific build image. If `--base-pkg` is used, it will replace the
   default list, i.e. `base-devel` has to be specified if it is required, too!
   This option may be specified multiple times.

-R <directory>, --rootfs <directory>

   Directory used to create the root filesystem for the base image with
   **pacstrap**. If not set, a new temporary directory will be created (using
   **mktemp**). Note: If set, the directory will be created if necessary, and
   no attempt will be made to remove it afterwards (including its contents).
   If not set, the temporary directory will be removed when it is no longer
   required.

-s, --sudo-docker

   Run **docker** commands using **sudo**. Note that even if this option is
   not set, **sudo** has to be configured properly for the user running
   **makedockerpkg**, since it is required for other commands (for example
   **pacstrap**). Running **makedockerpkg** itself as `root` is not
   recommended!

-u <user[:group]>, --user <user[:group]>

   User/group to run **makepkg** inside the container. These must be numerical
   values, since no user will be created inside the container. By default the
   UID/GID of the user running **makedockerpkg** will be used.

-U, --no-update

   Don't check the base image and the package-specific image for package
   updates.

-x, --execute

   Instead of running **makepkg** in the container, the *<makepkg options>*
   specify the command to run (and its parameters), e.g. to run a shell in the
   container, use:

   .. code::

      makedockerpkg --execute -- bash

   Note that environment variables from the command line will still be parsed!

DETAILS
==============================================================================

To build a package, **makedockerpkg** will create two Docker images: a base
image containing the `base-devel` group, and a package-specific build image
containing the dependencies required to build the package (this image will
also be created if there are no additional dependencies). The base image will
be used as the bottom layer for all package specific build images.

Both images will be checked for updates and inclusion of all required packages
before anything else, and the images will be recreated (from scratch) if
necessary. If the base image is recreated, the package-specific build images
will always be recreated, too. To be precise: **makedockerpkg** will check all
available tags of the parent (base) image, and if there is no `latest` tag,
the build image will be rebuilt, i.e. the `repo/name`\ s will be ignored for
this check!

*IMPORTANT:* Old unused images have to be removed manually, **makedockerpkg**
will never remove an image! If **makedockerpkg** dies unexpectedly, it may
also leave some containers behind which require manual deletion.

To create the base image, **makedockerpkg** will run **sudo pacstrap** to
create a new up-to-date Arch Linux rootfs. The locale will be set to
`en_US.UTF-8`, and **pacman**'s keyring will be initialized, otherwise this
rootfs will not be modified. The rootfs will then be used to create the image.

*NOTE:* **makedockerpkg** uses the `--squash` parameter of `docker build`,
which requires experimental features to be enabled. See the Docker
documentation for details.

The images to use may be set using the appropriate command line options, but
**makedockerpkg** will only check locally available images, and never pull any
remote images.

**makedockerpkg** will try to figure out which directories and/or files to
mount into the container during the build, e.g. `$PKGDEST`, but also local
package repositories listed in `pacman.conf`, and required configuration
files. Additional mounts may be added using the `--mount` option. The current
directory will be mounted as `/pkgbuild`, and this is where **makepkg** will
be run. The following directories and/or files will be mounted automatically:

   * local package repositories (`Server = file://…` in the **pacman** config)
   * **pacman** configuration (a temporary file created with **pacman-conf**)
   * **pacman** GnuPG keyring (if enabled with `--pacman-keys`)
   * **makepkg** configuration (global config, user config and/or the file
     specified on the command line)
   * directories specified in the environment variables `$PKGDEST`,
     `$SRCDEST`, `$SRCPKGDEST`, `$LOGDEST` and `$BUILDDIR`
   * the current working directory
   * directory specified in the `$GNUPGHOME` variable if enabled with
     `--gpg-mount`

The following environment variables will be set in the build container if they
are set and not empty on the host (to the same value as on the host):
`$BUILDDIR`, `$LOGDEST`, `$PKGDEST`, `$SOURCE_DATE_EPOCH`, `$SRCDEST` and
`$SRCPKGDEST`. `$GNUPGHOME` will only be set if the `--gpg-mount` option is
set (and `$GNUPGHOME` itself is set on the host). Apart from these variables,
the ones set by default by Docker and the variables set by **makepkg** itself
no environment variables will be set.

By default, **makepkg** will be executed as the current UID/GID, but no user
will be created in the container!

SEE ALSO
==============================================================================

*makepkg*\ (8), *makepkg.conf*\ (5), *pacman*\ (8), *pacman.conf*\ (5)

LICENSE
==============================================================================

Copyright © 2018-2019 Stefan Göbel < mkdkpk ʇɐ subtype ˙ de >.

**makedockerpkg** is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

**makedockerpkg** is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
**makedockerpkg**. If not, see <http://www.gnu.org/licenses/>.

.. :indentSize=3:tabSize=3:noTabs=true:mode=rest:maxLineLen=78:
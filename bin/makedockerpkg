#!/bin/bash

# Copyright 2018-2019 Stefan Göbel - < mkdkpk ʇɐ subtype ˙ de >.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see
# <http://www.gnu.org/licenses/>.

#------------------------------------------------------------------------------------------------------------------
#
# All sections between two #: markers will be included in the help output, in the order they appear in this script.
#
#------------------------------------------------------------------------------------------------------------------
#:
# Usage: makedockerpkg [<option>…] [-- <makepkg option>… [<ENVVAR>[+]=<value>]]
#
# Options:
#
#  -b --build-image <name>    docker build image [based on current dir's name]
#  -B --base-image <name>     base image [makedockerpkg/base-devel:latest]
#  -c --makepkg-conf <file>   makepkg.conf file [makepkg default location(s)]
#  -C --pacman-conf <file>    pacman config file [/etc/pacman.conf]
#  -d --debug                 print some status messages
#  -D --base-dir <directory>  base image build context [new temporary dir]
#  -f --force-build           force rebuild of build image
#  -F --force-base            force rebuild of base image
#  -g --gpg-mount             mount $GNUPGHOME in the build container
#  -G --pacman-keys           mount the host's pacman keyring in the container
#  -h --help                  show this help message
#  -m --mount <mount>         additional mounts for the build container
#  -o --no-color              disable colored output for -d/--debug
#  -p --build-pkg <package>   additional package to install in the build image
#  -P --base-pkg <package>    install this package in base image [base-devel]
#  -R --rootfs <directory>    rootfs dir for base image creation [new temp dir]
#  -s --sudo-docker           run docker via sudo
#  -u --user <user[:group]>   user to run makepkg as (numerical) [current user]
#  -U --no-update             don't check the images for package updates
#  -x --execute               run custom command instead of makepkg
#
# makedockerpkg must be run in the directory containing the PKGBUILD.
#
# Options -d/--debug, -m/mount <…>, -p/--build-pkg <…> and -P/--base-pkg <…>
# may be specified multiple times. Options to be passed to makepkg may be
# specified following the makedockerpkg options, separated by a double dash
# ("--"). In case -x/--execute is set, parameters following the separator
# specify the command to be executed instead of makepkg, and its parameters.
#:
#------------------------------------------------------------------------------------------------------------------

set -o errexit
set -o nounset
set -o pipefail

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_vars - Declare all our global variables, and save some environment variables for later. Should be
#                      called before anything else.
#
makedockerpkg_vars() {

   # shellcheck disable=SC2034
   declare -gr vers='0.3'     # The version of the script. Currently unused.

   declare -gA opts=()        # Command line options, plus some other values we need.
   declare -ga mpkg=()        # Additional makepkg command line options.

   declare -ga cmnt=()        # Mounts specified on the command line.
   declare -gA mnts=()        # Mounts for the containers (internally built array).

   declare -gA tmps=()        # Temporary files and directories, will be rm -rf'ed on exit.

   declare -ga pkg1=()        # Packages for the base image.
   declare -ga pkg2=()        # Packages for the build image.

   declare -gA envs=()        # Environment variables to set in the build container.

   local _name=''
   for _name in BUILDDIR GNUPGHOME LOGDEST PKGDEST SOURCE_DATE_EPOCH SRCDEST SRCPKGDEST ; do
      if [[ -n "${!_name:-}" ]] ; then
         envs["$_name"]="${!_name}"
      fi
   done

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_opts [<option>…] - Parse the (command line) options, which must be supplied as parameters to this
#                                  function. Results will be stored in some global variables (see above).
#
makedockerpkg_opts() {

   local _sopts='b:B:c:C:dD:fFgGhm:op:P:R:su:Ux'
   local _lopts=(
      'base-dir:'
      'base-image:'
      'base-pkg:'
      'build-image:'
      'build-pkg:'
      'debug'
      'execute'
      'force-base'
      'force-build'
      'gpg-mount'
      'help'
      'makepkg-conf:'
      'mount:'
      'no-color'
      'no-update'
      'pacman-conf:'
      'pacman-keys'
      'rootfs:'
      'sudo-docker'
      'user:'
   )

   local _gopts=''
   _gopts=$( getopt -n "$0" -o "$_sopts" -l "$( IFS=, ; printf '%s' "${_lopts[*]}" )" -- "$@" )

   eval set -- "$_gopts"

   opts=(
      ['base']='makedockerpkg/base-devel:latest'         # Base image name.
      ['bdir']=''                                        # Base image build context (directory), empty for mktemp.
      ['bldi']=''                                        # Build image name, based on working directory's name.
      ['date']=$( date '+%Y%m%d%H%M%S%N' )               # Current date and time (doesn't have an option).
      ['dlvl']='0'                                       # Debug level (verbosity).
      ['exec']=''                                        # Run extra args instead of makepkg if not empty.
      ['fbld']=''                                        # Force rebuild of build image if not empty.
      ['fbse']=''                                        # Force rebuild of base image if not empty.
      ['gpgm']=''                                        # Mount user's $GNUPGHOME if not empty.
      ['keys']=''                                        # Mount pacman GPG keyring if not empty.
      ['mcfg']=''                                        # makepkg config file (empty to use default locations).
      ['ncol']=''                                        # Disable colored output if not empty.
      ['nupd']=''                                        # Don't check for updates if not empty.
      ['pcfg']='/etc/pacman.conf'                        # pacman configuration file.
      ['root']=''                                        # Temporary rootfs location (for building the base image).
      ['serr']='/dev/null'                               # Redirection for STDERR (affected by debug level).
      ['sout']='/dev/null'                               # Redirection for STDOUT (affected by debug level).
      ['sudo']=''                                        # Command to run docker with (empty for none, or 'sudo').
      ['user']="$( id -u ):$( id -g )"                   # User to run makepkg as (numerical IDs only).
   )

   while true ; do
      case "$1" in
         -b | --build-image   )  opts['bldi']=$2         ;  shift 2  ;;
         -B | --base-image    )  opts['base']=$2         ;  shift 2  ;;
         -c | --makepkg-conf  )  opts['mcfg']=$2         ;  shift 2  ;;
         -C | --pacman-conf   )  opts['pcfg']=$2         ;  shift 2  ;;
         -D | --base-dir      )  opts['bdir']=$2         ;  shift 2  ;;
         -f | --force-build   )  opts['fbld']='1'        ;  shift    ;;
         -F | --force-base    )  opts['fbse']='1'        ;  shift    ;;
         -g | --gpg-mount     )  opts['gpgm']='1'        ;  shift    ;;
         -G | --pacman-keys   )  opts['keys']='1'        ;  shift    ;;
         -o | --no-color      )  opts['ncol']='1'        ;  shift    ;;
         -R | --rootfs        )  opts['root']=$2         ;  shift 2  ;;
         -s | --sudo-docker   )  opts['sudo']='sudo'     ;  shift    ;;
         -u | --user          )  opts['user']=$2         ;  shift 2  ;;
         -U | --no-update     )  opts['nupd']='1'        ;  shift    ;;
         -x | --execute       )  opts['exec']='1'        ;  shift    ;;
         -m | --mount         )  cmnt+=( "$2" )          ;  shift 2  ;;
         -p | --build-pkg     )  pkg2+=( "$2" )          ;  shift 2  ;;
         -P | --base-pkg      )  pkg1+=( "$2" )          ;  shift 2  ;;
         -d | --debug         )  (( ++ opts['dlvl'] ))   ;  shift    ;;
         -h | --help          )  makedockerpkg_help                  ;;
         --                   )  shift                   ;  break    ;;
         *                    )  makedockerpkg_help 1                ;;
      esac
   done

   # The remaining options will be passed to makepkg for the build (or run directly if -x/--execute is set):
   #
   mpkg=( "$@" )

   # Add the makepkg configuration file to the makepkg parameters if required:
   #
   if [[ -n "${opts[mcfg]}" ]] ; then
      mpkg+=( --config "${opts[mcfg]}" )
   fi

   # Resolve any package groups (required to check for installed packages). Using -P/--base-pkg will override what
   # would be installed in the base image, i.e. -P base-devel is required to install base-devel if any -P option is
   # used. For -p/--build-pkg, packages are always an addition to the build dependencies.
   #
   readarray -t pkg1 < <( makedockerpkg_rpkg "${pkg1[@]:-base-devel}" )
   readarray -t pkg2 < <( makedockerpkg_rpkg "${pkg2[@]}"             )

   # Build image name, in case it hasn't been set, based on the working directory's name. Need to clean it up, and
   # convert to lower case, cause Docker sucks.
   #
   if [[ -z "${opts[bldi]}" ]] ; then
      local _name=''
      _name=$( basename "$( pwd )" | tr -c '[:alnum:]' - | sed 's/\(^-*\|-*$\)//g' )
      opts['bldi']="makedockerpkg/${_name,,}:latest"
   fi

   # Add the :latest tag if the image names don't contain a tag.
   #
   if [[ "${opts[base]}" != ?*:?* ]] ; then opts['base']="${opts[base]}:latest" ; fi
   if [[ "${opts[bldi]}" != ?*:?* ]] ; then opts['bldi']="${opts[bldi]}:latest" ; fi

   # Remove some /dev/null output redirections for higher verbosity levels:
   #
   if (( opts['dlvl'] > 1 )) ; then opts['serr']='/dev/stderr' ; fi
   if (( opts['dlvl'] > 2 )) ; then opts['sout']='/dev/stdout' ; fi

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_help [<exit code>] - Print the help message and exit with the specified code (defaults to 0). All
#                                    sections of this script enclosed in two #: markers will be included, with the
#                                    leading "# " removed.
#
makedockerpkg_help() {

   if [[ -n "${1:-}" ]] ; then
      printf '\n'
   fi

   awk '/^#:$/{p=!p}p&&!/^#:$/{print substr($0,3)}' "${BASH_SOURCE[0]}"

   exit "${1:-0}"

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_prnt <level> <printf options> - Print the message if the verbosity/debug level is at least equal to
#                                               the specified level. Remaining parameters are passed to printf.
#
makedockerpkg_prnt() {

   local _lvl=$1
   shift

   local _bld=''
   local _col=''
   local _rst=''

   if [[ -z "${opts[ncol]}" ]] ; then
      _bld=$( tput bold    )
      _col=$( tput setaf 6 )
      _rst=$( tput sgr0    )
   fi

   if (( opts[dlvl] >= _lvl )) ; then

      if [[ -z "${opts[ncol]}" ]] ; then
         printf '%s%s' "$_bld" "$_col"
      fi

      # shellcheck disable=SC2059
      printf "$@"

      if [[ -z "${opts[ncol]}" ]] ; then
         printf '%s' "$_rst"
      fi

   fi

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_rpkg <package>… - Print a list of packages based on the supplied list, with groups replaced by the
#                                 individual member packages. This function calls pacman on the host. Packages will
#                                 be separated by newlines.
#
makedockerpkg_rpkg() {

   local _all=()
   local _grp=()
   local _cur=''

   for _cur in "$@" ; do
      if [[ -z "$_cur" ]] ; then
         continue
      fi
      readarray -t _grp < <( pacman -Sqg "$_cur" )
      _all+=( "${_grp[@]:-$_cur}" )
   done

   if (( ${#_all[@]} )) ; then
      printf '%s\n' "${_all[@]}"
   fi

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_drun <options> - Wrapper for `docker run`. Adds all mount options based on the global $mnts array
#                                plus any additional mounts. The docker command will be run via ${opts[sudo]}.
#
# These environment variables will be set in the container if they are set on the host (and not empty): $BUILDDIR,
# $LOGDEST, $PKGDEST, $SOURCE_DATE_EPOCH, $SRCDEST and $SRCPKGDEST (see makedockerpkg_vars). $GNUPGHOME will only
# be set if --gpg-mount/-g is set.
#
makedockerpkg_drun() {

   local _args=()
   local _mdst=''
   local _msrc=''
   local _ropt=''

   for _mdst in "${!mnts[@]}" ; do

      case "${mnts[$_mdst]:0:1}" in
         '0') _ropt=''      ;;
         '1') _ropt=',"ro"' ;;
      esac

      _msrc="${mnts[$_mdst]:2}"

      _mdst=${_mdst//\"/\"\"}
      _msrc=${_msrc//\"/\"\"}

      _args+=( --mount "\"type=bind\",\"src=$_msrc\",\"dst=$_mdst\"$_ropt" )

   done

   for _mdst in "${cmnt[@]}" ; do
      _args+=( --mount "$_mdst" )
   done

   local _name=''
   for _name in "${!envs[@]}" ; do
      if [[ "$_name" == 'GNUPGHOME' && -z "${opts[gpgm]}" ]] ; then
         continue
      fi
      _args+=( --env "$_name=${envs[$_name]}" )
   done

   ${opts[sudo]} docker run "${_args[@]}" "$@"

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_imgx <image> - Check if the specified Docker image exists (locally). Returns with an error if not.
#
makedockerpkg_imgx() {

   makedockerpkg_prnt 1 '?? Checking if image %s exists…\n' "$1"

   if ${opts[sudo]} docker image inspect "$1" >"${opts[sout]}" 2>"${opts[serr]}" ; then
      makedockerpkg_prnt 1 '== Image exists.\n'
      return 0
   else
      makedockerpkg_prnt 1 '== Image does not exist.\n'
      return 1
   fi

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_ltst <image> - Check if the parent image of the specified image has the "latest" tag. Important:
#                              repo/name will be ignored, i.e. any repo/name will work, as long as one entry is
#                              tagged as "latest"! Returns with success if the "latest" tag exists, with an error
#                              otherwise.
#
makedockerpkg_ltst() {

   makedockerpkg_prnt 1 '?? Checking if parent has been updated…\n' "$1"

   local _parent=''
   _parent=$( ${opts[sudo]} docker image inspect --format '{{.Parent}}' "$1" )

   local _output=''
   _output=$( ${opts[sudo]} docker image inspect --format '{{join .RepoTags "\n"}}' "$_parent" )

   local _rptags=()
   readarray -t _rptags < <( printf '%s' "$_output" )

   local _curtag=''
   for _curtag in "${_rptags[@]}" ; do
      if [[ "$_curtag" == *:latest ]] ; then
         makedockerpkg_prnt 1 '== Parent image is the latest.\n'
         return 0
      fi
   done

   makedockerpkg_prnt 1 '== Parent image has been updated.\n'
   return 1

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_updt <image> - Check if there are updates available for the packages of the image. Returns with an
#                              error if everything is up to date.
#
# If ${opts[nupd]} is set (i.e. the -U/--no-update option is present) this function will always return with an
# error (indicating no updates are available), i.e. this applies to both the base image and the build image. An
# error will also be returned if the check itself fails (e.g. when the network is down), but any attempted update
# would probably fail in that case anyway.
#
makedockerpkg_updt() {

   makedockerpkg_prnt 1 '?? Checking image %s for updates…\n' "$1"

   if [[ -n "${opts[nupd]}" ]] ; then
      makedockerpkg_prnt 1 '== Skipping.\n'
      return 1
   fi

   if makedockerpkg_drun --rm "$1" sh -c 'pacman -Sy && pacman -Qu' >"${opts[sout]}" 2>"${opts[serr]}" ; then
      makedockerpkg_prnt 1 '== Updates available.\n'
      return 0
   else
      makedockerpkg_prnt 1 '== No updates available.\n'
      return 1
   fi

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_pkgs <image> <package>… - Check if all listed packages are installed in the specified image.
#                                         Returns with an error if at least one package is missing. The package
#                                         list may be empty (note that in this case the pacman package will be
#                                         checked, though), and must not contain package groups.
#
makedockerpkg_pkgs() {

   local _img=$1
   shift

   makedockerpkg_prnt 1 '?? Checking image %s for required packages…\n' "$_img"

   local _arg=( --rm --user "${opts[user]}" )

   if makedockerpkg_drun "${_arg[@]}" "$_img" pacman -Qi "${@:-pacman}" >"${opts[sout]}" 2>"${opts[serr]}" ; then
      makedockerpkg_prnt 1 '== All packages present.\n'
      return 0
   else
      makedockerpkg_prnt 1 '== Missing package(s) found.\n'
      return 1
   fi

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_exit - To be set as EXIT… trap, deletes (with rm -rf) all elements of the $tmps associative array.
#
# This does not use sudo, for safety reasons, so the pacstrapped stuff can't be deleted. A message will be shown in
# that case.
#
makedockerpkg_exit() {

   local _exit=$?

   trap '' EXIT INT QUIT TERM
   set +o errexit

   local _file=''
   for _file in "${tmps[@]}" ; do
      rm -rf "$_file" >/dev/null 2>&1 || printf 'Could not remove temporary file or directory: %s\n' "$_file" >&2
   done

   exit "$_exit"

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_bdev - Bootstrap the rootfs and create the base-devel Docker image.
#
makedockerpkg_bdev() {

   makedockerpkg_prnt 1 ':: Building base image: %s.\n' "${opts[base]}"

   local _root=''
   _root=${opts[root]:-$( mktemp -d )}

   if [[ -z "${opts[root]}" ]] ; then
      tmps['rootfs']="$_root"
   else
      # shellcheck disable=SC2174
      mkdir -p -m 0700 "$_root"
   fi

   # Make some systemd crap happy:
   #
   sudo chown root:root "$_root"

   local _conf=''
   _conf=$( mktemp )
   tmps['pmconf']="$_conf"

   # Dump the config, remove some of the host paths and set the HookDir to /var/empty. This is for pacstrap only,
   # the full config will later be dumped to the new rootfs.
   #
   pacman-conf -c "${opts[pcfg]}" | \
      sed '/^RootDir\|^DBPath\|^GPGDir/d;s@^HookDir = .*$@HookDir = /var/empty@' >"$_conf"

   env -i sudo pacstrap -C "$_conf" -c -G -M "$_root" "${pkg1[@]}"

   rm -f "$_conf"
   unset 'tmps[pmconf]'

   # Following stuff is pretty much what's done to create the archlinux/base Docker image.

   printf 'LANG=en_US.UTF-8\n'  | sudo tee "$_root/etc/locale.conf" >/dev/null
   printf 'en_US.UTF-8 UTF-8\n' | sudo tee "$_root/etc/locale.gen"  >/dev/null

   sudo arch-chroot "$_root" locale-gen

   sudo arch-chroot "$_root" pacman-key --init
   sudo arch-chroot "$_root" pacman-key --populate archlinux

   # Temporary directory as build context (unless set on the command line):
   #
   local _base=''
   _base=${opts[bdir]:-$( mktemp -d )}

   if [[ -z "${opts[bdir]}" ]] ; then
      tmps['basedir']="$_base"
   else
      # shellcheck disable=SC2174
      mkdir -p -m 0700 "$_base"
   fi

   sudo tar --numeric-owner --xattrs --acls                 \
      '--exclude=.dockerenv'                                \
      '--exclude=.dockerinit'                               \
      '--exclude=etc/hostname'                              \
      '--exclude=etc/machine-id'                            \
      '--exclude=etc/pacman.d/gnupg/openpgp-revocs.d/*'     \
      '--exclude=etc/pacman.d/gnupg/private-keys-v1.d/*'    \
      '--exclude=etc/pacman.d/gnupg/pubring.gpg~'           \
      '--exclude=etc/pacman.d/gnupg/S.*'                    \
      '--exclude=root/*'                                    \
      '--exclude=tmp/*'                                     \
      '--exclude=var/cache/pacman/pkg/*'                    \
      '--exclude=var/lib/pacman/sync/*'                     \
      '--exclude=var/tmp/*'                                 \
      -C "$_root" -c . -f "$_base/rootfs.tar"

   # We only need the tar archive, delete the rootfs if necessary.
   #
   if [[ -z "${opts[root]}" ]] ; then
      sudo rm -rf "$_root"
      unset 'tmps[rootfs]'
   fi

   sed 's/^ *//' >"$_base/Dockerfile" <<'______EOF'
      FROM scratch
      ADD rootfs.tar /
      ENV LANG=en_US.UTF-8
______EOF

   ${opts[sudo]} docker build --squash -t "${opts[base]}" -t "${opts[base]/:*/:${opts[date]}}" "$_base"

   if [[ -z "${opts[bdir]}" ]] ; then
      rm -rf "$_base"
      unset 'tmps[basedir]'
   fi

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_bldi <name> - Create the specified build image.
#
# This will run the base container, install the build dependencies and commit the changes to the build image. The
# reason we do not use a Dockerfile and RUN pacman -S … in a real build is that we can't have bind mounts during
# build (Docker sucks…), but for local repos (i.e. Server = file://… in pacman.conf) and/or if we need to mount the
# host's pacman keyring, we need these, otherwise pacman will fail (and not be able to install dependencies from
# these local repos). The resulting image should be the same as one FROM the base image with the matching RUN
# option.
#
makedockerpkg_bldi() {

   makedockerpkg_prnt 1 ':: Building build image: %s.\n' "${opts[bldi]}"

   local _deps=''
   _deps=$( makedockerpkg_deps )

   local _pkgs=()
   readarray -t _pkgs < <( printf '%s' "$_deps" )
   _pkgs+=( "${pkg2[@]}" pacman )

   local _ctid=''
   _ctid=$( uuidgen -r )

   local _quot=''
   _quot=$( printf " '%s' " "${_pkgs[@]}" )

   makedockerpkg_drun --name "$_ctid" "${opts[base]}" \
      bash -c "pacman --noconfirm -Syu && pacman --noconfirm --needed -S $_quot && { yes | LC_ALL=C pacman -Scc ;}"

   ${opts[sudo]} docker container commit "$_ctid" "${opts[bldi]}"
   ${opts[sudo]} docker container rm     "$_ctid"

   ${opts[sudo]} docker tag "${opts[bldi]}" "${opts[bldi]/:*/:${opts[date]}}"

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_deps - Print the list of dependencies for the build image (separated by newlines).
#
makedockerpkg_deps() {

   local _pwd=''
   _pwd=$( pwd )
   _pwd=${_pwd//\"/\"\"}

   local _mnt="\"type=bind\",\"src=$_pwd\",\"dst=/pkgbuild\""

   makedockerpkg_drun --rm --user "${opts[user]}" --workdir /pkgbuild --mount "$_mnt"   \
      --env PACMAN=true --env HOME=/ "${opts[base]}" makepkg --printsrcinfo           | \
      awk '/^\s+(make|check)?depends\s+=/{print$3}'

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_pmnt - Add mounts for pacman (local repos, keyring if enabled) to the global $mnts array. A
#                      temporary file will be created to dump the pacman configuration, this will be mounted, too.
#
makedockerpkg_pmnt() {

   local _arg=( --rm --user "${opts[user]}" "${opts[base]}" )
   local _cfg=''

   # Temporary pacman.conf:

   _cfg=$( mktemp )
   tmps['pacman.conf']=$_cfg

   pacman-conf -c "${opts[pcfg]}" >"$_cfg"
   mnts['/etc/pacman.conf']="1:$_cfg"

   # Local repository directories:

   local _mnt=()
   readarray -t _mnt < <( awk '/^Server = file:\/\/\//{print substr($3,8)}' "$_cfg" | sed 's!/*$!!' | sort -u )

   local _dir=''
   for _dir in "${_mnt[@]}" ; do
      mnts["$_dir"]="1:$_dir"
   done

   # GPG keyring (if enabled):

   if [[ -n "${opts[keys]}" ]] ; then
      _dir=$( pacman-conf -c "${opts[pcfg]}" GPGDir )
      mnts["$_dir"]="1:$_dir"
   fi

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_cloc - Print the location of the user's makepkg.conf (and return with success), or nothing if none
#                      is found (and return with an error).
#
makedockerpkg_cloc() {

   if [[ -r "${XDG_CONFIG_HOME:-$HOME/.config}/pacman/makepkg.conf" ]] ; then

      printf '%s' "${XDG_CONFIG_HOME:-$HOME/.config}/pacman/makepkg.conf"
      return 0

   elif [[ -r "$HOME/.makepkg.conf" ]] ; then

      printf '%s' "$HOME/.makepkg.conf"
      return 0

   fi

   return 1

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_mmnt - Add mounts for makepkg (for all $*DEST directories, $BUILDDIR, $GNUPGHOME and makepkg.conf,
#                      if necessary). See makedockerpkg_dsts below for details on getting the values.
#
makedockerpkg_mmnt() {

   makedockerpkg_dsts "${mpkg[@]}"

   local _dir=''

   # These will be automatically mounted (rw) at the same path in the container as on the host…
   #
   for _dir in PKGDEST SRCDEST SRCPKGDEST LOGDEST BUILDDIR ; do
      if [[ -n "${!_dir:-}" ]] ; then
         mnts["${!_dir}"]="0:${!_dir}"
      fi
   done

   # …and the current directory (containing the PKGBUILD) will be mounted at /pkgbuild.
   #
   mnts['/pkgbuild']="0:$( pwd )"

   # Global makepkg.conf (always mount it, even if it is not used):
   #
   mnts['/etc/makepkg.conf']='0:/etc/makepkg.conf'

   # The user's own makepkg.conf. If one has been set on the command line, mount that one in the container (at the
   # same path as on the host). Otherwise, check the default locations, the first one found will be mounted to
   # /.makepkg.conf (the user will not have a real $HOME in the container, it will be / instead; and we don't need
   # both files since makepkg will only use the first one found, anyway).
   #
   if [[ -n "${opts[mcfg]}" && "${opts[mcfg]}" != '/etc/makepkg.conf' ]] ; then
      mnts["${opts[mcfg]}"]="1:${opts[mcfg]}"
   else
      local _usr=''
      if _usr=$( makedockerpkg_cloc ) ; then
         mnts['/.makepkg.conf']="1:$_usr"
      fi
   fi

   # Mount $GNUPGHOME if set and enabled (has to be rw to work without additional gpg-agent configuration).
   #
   if [[ -n "${opts[gpgm]}" && -n "$GNUPGHOME" ]] ; then
      mnts["$GNUPGHOME"]="0:$GNUPGHOME"
   fi

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_dsts [<option>…] - Get the output directories for makepkg. Options should be the ones remaining
#                                  after command line parsing (i.e. everything in the global $mpkg array after
#                                  parse_options has been called).
#
# This function basically does the same as makepkg itself to get the directories - from the global config, user
# config, environment variables and command line overrides (in that order, later values override previous ones).
#
# The global variables $PKGDEST, $SRCDEST, $SRCPKGDEST, $LOGDEST, $BUILDDIR, $GNUPGHOME and $MAKEPKG_CONF will be
# set after this function has been called. It will also declare all variables set on the command line for makepkg!
#
makedockerpkg_dsts() {

   local _current_env=''
   local _commandline=()

   _current_env=$( declare -p PKGDEST SRCDEST SRCPKGDEST LOGDEST BUILDDIR GNUPGHOME 2>/dev/null || true )

   shopt -s extglob

   # shellcheck disable=SC2178
   local _arg=''
   for _arg ; do
      if [[ "$_arg" = [_[:alpha:]]*([[:alnum:]_])?(+)=* ]] ; then
         _commandline+=( "$_arg" )
      fi
   done

   if [[ -n "${opts[mcfg]}" ]] ; then
      declare -g MAKEPKG_CONF=${opts[mcfg]}
   else
      declare -g MAKEPKG_CONF=${MAKEPKG_CONF:-/etc/makepkg.conf}
   fi

   shopt -u extglob

   # shellcheck disable=SC1090
   source "$MAKEPKG_CONF"

   if [[ "$MAKEPKG_CONF" == '/etc/makepkg.conf' ]] ; then
      local _usr=''
      if _usr=$( makedockerpkg_cloc ) ; then
         # shellcheck disable=SC1090
         source "$_usr"
      fi
   fi

   shopt -s extglob

   eval "$_current_env"

   if (( ${#_commandline[@]} )) ; then
      declare -g "${_commandline[@]}"
   fi

   local _curdir=''
   local _tmpdir=''
   local _wrkdir=''

   _wrkdir=$( pwd )
   for _curdir in PKGDEST SRCDEST SRCPKGDEST LOGDEST BUILDDIR ; do
      _tmpdir="$( realpath -e "${!_curdir:-$_wrkdir}" )"
      printf -v "$_curdir" '%s' "$_tmpdir"
   done

   if [[ -n "$GNUPGHOME" ]] ; then
      GNUPGHOME=$( realpath -e "$GNUPGHOME" )
   elif [[ -e "$HOME/.gnupg" ]] ; then
      GNUPGHOME=$( realpath -e "$HOME/.gnupg" )
   fi

}

#------------------------------------------------------------------------------------------------------------------
#
# makedockerpkg_main [<option> …] - Main… parameters must be the command line options.
#
makedockerpkg_main() {

   makedockerpkg_vars
   makedockerpkg_opts "$@"

   trap makedockerpkg_exit EXIT INT QUIT TERM

   # Check and build the base image if necessary. If the base image already exists, set up the pacman mounts before
   # checking for updates and installed packages (required for local file:// repos during update/package check). In
   # any case (re)initialize the pacman mounts after the (re)build.

   local _build=${opts[fbse]:-0}

   if (( ! _build )) && ! makedockerpkg_imgx "${opts[base]}"              ; then (( ++ _build ))     ; fi
   if (( ! _build ))                                                      ; then makedockerpkg_pmnt  ; fi
   if (( ! _build )) &&   makedockerpkg_updt "${opts[base]}"              ; then (( ++ _build ))     ; fi
   if (( ! _build )) && ! makedockerpkg_pkgs "${opts[base]}" "${pkg1[@]}" ; then (( ++ _build ))     ; fi

   if (( _build )) ; then
      makedockerpkg_bdev
      declare -gA mnts=()
      makedockerpkg_pmnt
   fi

   # Check and build the build image if necessary, then set up the makepkg mounts. $_build will be reset, so the
   # build image will not automatically be rebuild when the base image has been rebuild.

   local _build=${opts[fbld]:-0}
   local _depkg=()

   readarray -t _depkg < <( makedockerpkg_deps )
   _depkg+=( "${pkg2[@]}" )

   if (( ! _build )) && ! makedockerpkg_imgx "${opts[bldi]}"                ; then (( ++ _build )) ; fi
   if (( ! _build )) && ! makedockerpkg_ltst "${opts[bldi]}"                ; then (( ++ _build )) ; fi
   if (( ! _build )) &&   makedockerpkg_updt "${opts[bldi]}"                ; then (( ++ _build )) ; fi
   if (( ! _build )) && ! makedockerpkg_pkgs "${opts[bldi]}" "${_depkg[@]}" ; then (( ++ _build )) ; fi

   if (( _build )) ; then
      makedockerpkg_bldi
   fi

   makedockerpkg_mmnt

   # Run the build (or the custom command if requested).

   local _args=( --rm --user "${opts[user]}" --env 'HOME=/' --workdir /pkgbuild -ti "${opts[bldi]}" )

   if [[ -z "${opts[exec]}" ]] ; then
      makedockerpkg_drun "${_args[@]}" makepkg "${mpkg[@]}"
   else
      makedockerpkg_drun "${_args[@]}"         "${mpkg[@]}"
   fi

}

#------------------------------------------------------------------------------------------------------------------

if [[ "$0" == "${BASH_SOURCE[0]}" ]] ; then
   makedockerpkg_main "$@"
fi

# :indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=115: ############################################